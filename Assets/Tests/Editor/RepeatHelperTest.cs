using Helpers;
using NUnit.Framework;
using UnityEngine;

public class RepeatHelperTest
{
    private Rect _testRect = new (0, 0, 10, 10);
    private readonly Rect _boxRect = new (0, 0, 100, 100);
    private readonly Vector2 _halfScreenSize = new (90f, 50f);
    private readonly Vector2 _shipSize = new (10.0f, 12.0f);

    [Test]
    [TestCase(0, 0, true)]
    [TestCase(0, 90, true)]
    [TestCase(50, 50, true)]
    [TestCase(90, 50, true)]
    [TestCase(91, 90, false)]
    [TestCase(102, 0, false)]
    [TestCase(-2, 0, false)]
    [TestCase(-200, -100, false)]

    public void Contains_ShouldReturnExpectedResult(float x, float y, bool result)
    {
        _testRect.x = x;
        _testRect.y = y;
        Assert.AreEqual(_boxRect.Contains(_testRect.position, _testRect.size), result);
    }

    [Test]
    [TestCase(0, 48, false)]
    [TestCase(0, 0, false)]
    [TestCase(0, 52, true)]
    [TestCase(-91, 0, true)]
    [TestCase(91, 0, true)]
    [TestCase(0, -51, true)]
    [TestCase(-89, 0, false)]
    [TestCase(89, 49, false)]

    public void CalculateRepeat_ShouldReturnExpectedResult(float x, float y, bool result)
    {
        var z = new Vector2(x, y);
        var position = z;
        RepeatHelper.CalculateRepeat(_halfScreenSize, ref position);

        Assert.AreEqual(z != position, result);
    }

    [Test]
    [TestCase(10, 10, 0, 0)]
    [TestCase(85, 0, -180f, 0)]
    [TestCase(-85, 0, 180f, 0)]
    [TestCase(0, 48, 0, -100f)]
    [TestCase(0, -48, 0, 100f)]
    public void CalculateOffset_Should(float  x, float y, float expectedX, float expectedY)
    {
        var offset = RepeatHelper.CalculateOffset(_halfScreenSize, new Vector2(x, y), _shipSize);
        Assert.AreEqual(expectedX, offset.x);
        Assert.AreEqual(expectedY, offset.y);
    }
}
