using System.Collections;
using Code.Settings;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class WeaponTest
{
    private Transform _transform;

    private IWeapon _weapon;

    [OneTimeSetUp]
    public void SetupOnce()
    {
        Service.Register<IFactory, FakeFactory>();
    }

    [UnitySetUp]
    public IEnumerator Setup()
    {
        yield return new WaitForEndOfFrame();
        _transform = new GameObject("WeaponRoot").transform;
        _weapon  = new StandardWeapon(new WeaponSettings(){ CoolDown = 0.1f}, _transform);
    }

    [TearDown]
    public void Finish()
    {
    }

    [UnityTest]
    public IEnumerator StartFire_ShouldCreateBullets()
    {
        _weapon.StartFire();
        yield return null;
        Assert.NotZero(_weapon.CountActiveBullets);
    }

    [UnityTest]
    public IEnumerator StopFire_ShouldStopCreateNewBullets()
    {
        _weapon.StartFire();
        var x = 1.0f;
        while (x > 0)
        {
            x -= Time.deltaTime;
            _weapon.Update(Time.deltaTime);
            yield return null;
        }
        _weapon.StopFire();
        var count = _weapon.CountActiveBullets;
        x = 1.0f;
        while (x > 0)
        {
            x -= Time.deltaTime;
            _weapon.Update(Time.deltaTime);
            yield return null;
        }

        yield return null;
        Assert.AreEqual(count, _weapon.CountActiveBullets);
    }

    
    [UnityTest]
    public IEnumerator StartFire_ShouldCreateManyBullets()
    {
        _weapon.StartFire();
        var x = 1.0f;
        while (x > 0)
        {
            x -= Time.deltaTime;
            _weapon.Update(Time.deltaTime);
            yield return null;
        }
        yield return null;
        Assert.Greater(_weapon.CountActiveBullets, 4);
    }
}
