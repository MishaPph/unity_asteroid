using System.Collections;
using System.Threading.Tasks;
using Code.Enums;
using Code.Repositories;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class BulletTest
{
    private Bullet _bullet;

    private static Asteroid CreateAsteroid(Vector2 position)
    {
        var asteroid = Service.Get<IFactory>().CreateAsteroid(AsteroidId.Small);
        asteroid.Settings = Object.Instantiate(asteroid.Settings);
        asteroid.Settings.Health = 1;
        asteroid.SetPosition(position);
        return asteroid;
    }

    [OneTimeSetUp]
    public void SetupOnce()
    {
    }

    private async Task Load()
    {
        Service.Register<IFactory, Factory>();
        await RepositoryManager.Instance.Load<BulletRepository>();
        await RepositoryManager.Instance.Load<AsteroidRepository>();
    }

    [UnitySetUp]
    public IEnumerator Setup()
    {
        if (!RepositoryManager.Instance.HasRepository<BulletRepository>())
        {
            Service.Register<IFactory, Factory>();
            var task = Load();
            task.SafeFireAndForget();
            while (!task.IsCompleted)
            {
                yield return new WaitForSeconds(0.1f);
            }
        }
        _bullet = Service.Get<IFactory>().CreateBullet(BulletId.Default);
        _bullet.Life = 50.0f;
        _bullet.SetPosition(Vector2.zero);
    }
    
    [TearDown]
    public void Finish()
    {
        Object.Destroy(_bullet.Fake);
        Object.Destroy(_bullet.Active);
    }

    [UnityTest]
    public IEnumerator Move_ShouldDecreaseLifeByDeltaTime()
    {
        const float start = 1.0f;
        const float delta = 0.5f;
        _bullet.Life = start;
        _bullet.Move(delta);
        yield return null;

        Assert.AreEqual((start - delta), _bullet.Life);
    }

    [UnityTest]
    public IEnumerator Move_ShouldChangePosition()
    {
        _bullet.Velocity = Vector2.up;
        _bullet.Move(1.0f);
        yield return new WaitForEndOfFrame();
        Assert.AreEqual(Vector2.up, _bullet.Position);
    }

    [UnityTest]
    public IEnumerator Triggered_WhenCollisionAsteroid_ShouldHitAsteroid()
    {
        var asteroid = CreateAsteroid(new Vector2(0, 10));
        _bullet.Velocity = Vector2.up;
        bool hitted = false;
        _bullet.Triggered += (_, _) =>
        {
            hitted = true;
        };
        int k = 0;
        while (k++ < 10 && !hitted)
        {
            _bullet.Move(1.0f);
            yield return new WaitForFixedUpdate(); 
        }
        
        Object.Destroy(asteroid.Active);
        Object.Destroy(asteroid.Fake);

        Assert.IsTrue(hitted);
    }
}