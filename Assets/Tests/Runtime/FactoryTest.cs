using System.Collections;
using System.Threading.Tasks;
using Code.Enums;
using Code.Repositories;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class FactoryTest
{
    private Transform _transform;

    private async Task Load()
    {
        Service.Register<IFactory, Factory>();
        await RepositoryManager.Instance.Load<WeaponRepository>();
        await RepositoryManager.Instance.Load<BulletRepository>();
        await RepositoryManager.Instance.Load<ShipRepository>();
        await RepositoryManager.Instance.Load<AsteroidRepository>();
    }

    [UnitySetUp]
    public IEnumerator SetupTest()
    {
        _transform = new GameObject("WeaponRoot").transform;
        if (!RepositoryManager.Instance.HasRepository<WeaponRepository>())
        {
            var task = Load();
            task.SafeFireAndForget();
            while (!task.IsCompleted)
            {
                yield return new WaitForSeconds(0.1f);
            }
        }
    }

    [Test]
    [TestCase(WeaponId.Standard)]
    public void CreateWeapon_ShouldCreateWeapon(WeaponId weaponId)
    {
        var weapon = Service.Get<IFactory>().CreateWeapon(weaponId, _transform);
        Assert.NotNull(weapon);
    }

    [Test]
    [TestCase(BulletId.Small)]
    [TestCase(BulletId.Big)]
    [TestCase(BulletId.Default)]
    public void CreateBullet_ShouldCreateWeapon(BulletId id)
    {
        var bullet = Service.Get<IFactory>().CreateBullet(id);
        Assert.NotNull(bullet);
    }

    [Test]
    [TestCase(AsteroidId.Small)]
    [TestCase(AsteroidId.Middle)]
    [TestCase(AsteroidId.Big)]
    [TestCase(AsteroidId.Huge)]
    public void CreateAsteroid_ShouldCreateAsteroid(AsteroidId id)
    {
        var asteroid = Service.Get<IFactory>().CreateAsteroid(id);
        Assert.NotNull(asteroid);
    }
}