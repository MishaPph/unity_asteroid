using Code.Enums;
using UnityEngine;

internal class FakeFactory : IFactory
{
    public IWeapon CreateWeapon(WeaponId weaponId, Transform root)
    {
        throw new System.NotImplementedException();
    }

    public UnitModel CreateShip(ShipId id)
    {
        throw new System.NotImplementedException();
    }

    public Bullet CreateBullet(BulletId id)
    {
        return new Bullet(new GameObject("Bullet"), Vector2.zero);
    }

    public Asteroid CreateAsteroid(AsteroidId id)
    {
        throw new System.NotImplementedException();
    }
}