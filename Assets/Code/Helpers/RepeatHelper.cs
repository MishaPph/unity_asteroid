using UnityEngine;

namespace Helpers
{
    public static class RepeatHelper
    {
        public static bool Contains(this Rect rect, Vector2 position, Vector2 size)
        {
            if ((rect.x) > position.x || (rect.y) > position.y)
                return false;
            if ((rect.x + rect.size.x) < (position.x + size.x) || (rect.y + rect.size.y) < (position.y + size.y))
            {
                return false;
            }
            return true;
        }

        public static void CalculateRepeat(Vector2 halfBoxSize, ref Vector2 position)
        {
            if (position.y > halfBoxSize.y)
            {
                position.y -= halfBoxSize.y * 2;
                return;
            }
            if (position.y < -halfBoxSize.y)
            {
                position.y += halfBoxSize.y * 2;
                return;
            }
            if (position.x > halfBoxSize.x)
            {
                position.x -= halfBoxSize.x * 2;
                return;
            }
            if (position.x < -halfBoxSize.x)
            {
                position.x += halfBoxSize.x * 2;
                return;
            }
        }

        public static Vector2 CalculateOffset(Vector2 halfScreenSize, Vector2 position, Vector2 objectSize)
        {
            if (position.y + objectSize.y > halfScreenSize.y)
            {
                return new Vector2(0, -halfScreenSize.y* 2);
            }
            if (position.y - objectSize.y < -halfScreenSize.y)
            {
                return new Vector2(0, halfScreenSize.y* 2);
            }
            if (position.x + objectSize.x > halfScreenSize.x)
            {
                return new Vector2(-halfScreenSize.x* 2, 0);
            }
            if (position.x - objectSize.x < -halfScreenSize.x)
            {
                return new Vector2(+halfScreenSize.x* 2, 0);
            }
            return Vector2.zero;
        }
    }
}