using System;
using System.Threading.Tasks;
using UnityEngine;

public static class TaskHelper
{
    public static async void SafeFireAndForget(this Task task, bool continueOnCapturedContext = true)
    {
        try
        {
            await task.ConfigureAwait(continueOnCapturedContext);
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
        }
    }
}