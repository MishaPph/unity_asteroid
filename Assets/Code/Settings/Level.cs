﻿using System;
using Code.Enums;
using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "Settings/Level")]
public class Level : ScriptableObject
{
    [Serializable]
    public struct Info
    {
        public int Count;
        public AsteroidId Id;
        [MinMaxSlider(0.3f, 0.9f)]
        public Vector2 Range;
        [MinMaxSlider(1f, 20f)]
        public Vector2 Speed;
    }

    public Info[] Asteroids;
}