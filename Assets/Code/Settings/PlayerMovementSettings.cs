using UnityEngine;

namespace Code.Settings
{
    [CreateAssetMenu(fileName = "PlayerMovementSettings", menuName = "Settings/PlayerMovementSettings", order = 1)]
    public class PlayerMovementSettings : ScriptableObject
    {
        [Header("Movement (in m/s)")]
        public AnimationCurve AccelerationCurve;
        public AnimationCurve DecelerationCurve;
        [Range(0.0f, 120.0f)] public float Speed;

        [Header("Turn")]
        public float NotTurningThreshold;
        public AnimationCurve MaxTurnCurve;
        [Range(0.0f, 72.0f)] public float TurnSpeed = 1.0f;
    }
}