using Code.Enums;
using UnityEngine;

namespace Code.Settings 
{
    [CreateAssetMenu(fileName = "AsteroidSettings", menuName = "Settings/AsteroidSettings", order = 1)]
    public class AsteroidSettings : ScriptableObject
    {
        public AsteroidId Id;
        public GameObject Prefab;
        public float Health = 40f;
        public ParticleSystem DestroyedPrefabFx;

        [Range(0, 1000)] 
        public int Mass = 1;
        public int MaxTurnSpeed = 180;

        public AsteroidId[] PartIds;
    }
}