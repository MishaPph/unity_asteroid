using Code.Enums;
using UnityEngine;

namespace Code.Settings
{
    [CreateAssetMenu(fileName = "WeaponSettings", menuName = "Settings/WeaponSettings", order = 1)]
    public class WeaponSettings : ScriptableObject
    {
        public float CoolDown = 0.2f;
        public float Damage = 10;
        public BulletId BulletId;
        public ParticleSystem HitPrefabFx;
        public float BulletLifeTime = 5;
        public float BulletSpeed = 10;
    }
}