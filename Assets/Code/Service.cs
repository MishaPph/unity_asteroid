using System;
using System.Collections.Generic;

public static class Service
{
    private static readonly Dictionary<Type, object> _dictionary = new Dictionary<Type, object>();
    public static void Register<T, TD>() where TD : T
    {
        _dictionary.Remove(typeof(T));
        _dictionary.Add(typeof(T), Activator.CreateInstance<TD>());
    }

    public static T Get<T>()
    {
        return (T)_dictionary[typeof(T)];
    }
}