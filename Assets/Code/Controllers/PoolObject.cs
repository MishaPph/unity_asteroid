using System;
using System.Collections.Generic;

public class PoolObject<T, TK> where T : IPoolObject
{
    private readonly Queue<T> _free = new ();
    private readonly Func<TK, T> _create;
    private readonly TK _prefab;

    public PoolObject(TK prefab, Func<TK, T> create)
    {
        _create = create;
        _prefab = prefab;
    }

    public T Create()
    {
        if (_free.Count == 0) 
            return _create.Invoke(_prefab);
        var o = _free.Dequeue();
        o.Activate();
        return o;
    }

    public void Return(T t)
    {
        t.Deactivate();
        _free.Enqueue(t);
    }
}