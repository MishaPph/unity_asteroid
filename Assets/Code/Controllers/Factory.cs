using System.Collections.Generic;
using Code.Enums;
using Code.Repositories;
using UnityEngine;

public interface IFactory
{
    IWeapon CreateWeapon(WeaponId weaponId, Transform root);
    UnitModel CreateShip(ShipId id);
    Bullet CreateBullet(BulletId id);
    Asteroid CreateAsteroid(AsteroidId id);
}

public class Factory : IFactory
{
    public IWeapon CreateWeapon(WeaponId weaponId, Transform root)
    {
        if (RepositoryManager.Instance.GetRepository<WeaponRepository>().TryGet(weaponId, out var settings))
        {
            return new StandardWeapon(settings, root);
        }
        throw new KeyNotFoundException($"Weapon setting for {weaponId} not found in WeaponsCollection");
    }

    public UnitModel CreateShip(ShipId id)
    {
        if (RepositoryManager.Instance.GetRepository<ShipRepository>().TryGet(id, out var prefab))
        {
            return new UnitModel(prefab, prefab.GetComponent<SpriteRenderer>().bounds.size);
        }
        throw new KeyNotFoundException($"Ship prefab {id} not found in ShipRepository");
    }

    public Bullet CreateBullet(BulletId id)
    {
        if (RepositoryManager.Instance.GetRepository<BulletRepository>().TryGet(id, out var prefab))
        {
            return new Bullet(prefab, prefab.GetComponent<SpriteRenderer>().bounds.size);
        }
        throw new KeyNotFoundException($"Bullet prefab {id} not found in BulletRepository");
    }

    public Asteroid CreateAsteroid(AsteroidId id)
    {
        if (RepositoryManager.Instance.GetRepository<AsteroidRepository>().TryGet(id, out var settings))
        {
            var a = new Asteroid(settings.Prefab, settings.Prefab.GetComponent<SpriteRenderer>().bounds.size)
            {
                Settings = settings
            };
            return a;
        }
        throw new KeyNotFoundException($"Asteroid setting {id} not found in AsteroidRepository");
    }
}