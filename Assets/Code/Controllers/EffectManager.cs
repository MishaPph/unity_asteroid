using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EffectManager
{
    private static EffectManager _instance;
    public static EffectManager Instance => _instance ??= new EffectManager();

    private readonly Dictionary<GameObject, Queue<GameObject>> _objects = new ();

    public T Play<T>(GameObject prefab, Vector2 position) where T : MonoBehaviour
    {
        if (!_objects.TryGetValue(prefab, out var queue) || queue.Count == 0)
            return Object.Instantiate(prefab, position, Quaternion.identity).GetComponent<T>();

        var l = queue.Dequeue();
        l.SetActive(true);
        l.transform.localPosition = position;
        return l.GetComponent<T>();
    }

    public void Return<T>(GameObject prefab, T data) where T : MonoBehaviour
    {
        data.gameObject.SetActive(false);
        if (_objects.TryGetValue(prefab, out var queue))
        {
            queue.Enqueue(data.gameObject);
        }
        else
        {
            var q = new Queue<GameObject>();
            q.Enqueue(data.gameObject);
            _objects.Add(prefab, q);
        }

    }

    public void Clear()
    {
        foreach (var obj in _objects.SelectMany(pair => pair.Value))
        {
            Object.Destroy(obj);
        }

        _objects.Clear();
    }
}
