using System.Collections.Generic;
using Code.Enums;
using Code.Observers;
using Code.Repositories;
using UnityEngine;

public class AsteroidManager : MonoBehaviour, IAsteroidDeadObserver, ILevelStartObserver, IGameEndObserver
{
    private readonly Dictionary<AsteroidId, PoolObject<Asteroid, AsteroidId>> _asteroids = new();
    private readonly List<Asteroid> _activeAsteroids = new();

    private void OnEnable()
    {
        ObserverCollection.Instance.Register<IAsteroidDeadObserver>(this);
        ObserverCollection.Instance.Register<ILevelStartObserver>(this);
        ObserverCollection.Instance.Register<IGameEndObserver>(this);
    }

    private void OnDisable()
    {
        ObserverCollection.Instance.Unregister<IAsteroidDeadObserver>(this);
        ObserverCollection.Instance.Unregister<ILevelStartObserver>(this);
        ObserverCollection.Instance.Unregister<IGameEndObserver>(this);
    }

    private void Update()
    {
        foreach (var asteroid in _activeAsteroids)
        {
            asteroid.Move(Time.deltaTime);
        }
    }

    private void SpawnLevel(Level level)
    {
        Clear();
        foreach (var info in level.Asteroids)
        {
            for (int i = 0; i < info.Count; i++)
            {
                Spawn(info);
            }
        }
    }

    private Asteroid Spawn(Level.Info info)
    {
        var newVelocity = (Vector2)(Quaternion.Euler(0, 0, Random.Range(-180, 180)) * Vector3.up);
        var positionX = GameScreen.HalfSize.x * Random.Range(info.Range.x, info.Range.y);
        if (Random.value < 0.5f)
            positionX *= -1;

        var positionY = GameScreen.HalfSize.y * Random.Range(info.Range.x, info.Range.y);
        if (Random.value < 0.5f)
            positionY *= -1;

        return Spawn(info.Id, new Vector2(positionX, positionY), newVelocity * Random.Range(info.Speed.x, info.Speed.y));
    }

    private Asteroid Spawn(AsteroidId id, Vector2 position, Vector2 velocity)
    {
        var asteroid =  Initiate(id);
        asteroid.SetTransform(position, Quaternion.identity);
        asteroid.Velocity = velocity;
        asteroid.TurnSpeed = Random.Range(-asteroid.Settings.MaxTurnSpeed, asteroid.Settings.MaxTurnSpeed);
        _activeAsteroids.Add(asteroid);
        return asteroid;
    }

    private Asteroid Initiate(AsteroidId asteroidId)
    {
        if (_asteroids.TryGetValue(asteroidId, out var pool))
        {
            return pool.Create();
        }

        if (RepositoryManager.Instance.GetRepository<AsteroidRepository>().TryGet(asteroidId, out var settings))
        {
            var p = new PoolObject<Asteroid, AsteroidId>(settings.Id, Service.Get<IFactory>().CreateAsteroid);
            _asteroids.Add(asteroidId, p);
            return p.Create();
        }

        throw new KeyNotFoundException($"AsteroidId {asteroidId} not found");
    }

    public void OnAsteroidDead(Asteroid asteroid)
    {
        _activeAsteroids.Remove(asteroid);
        if (_asteroids.TryGetValue(asteroid.Settings.Id, out var pool))
        {
            pool.Return(asteroid);
        }

        if (asteroid.Settings.PartIds.Length > 0)
        {
            GenerateAsteroidsAfterDestroy(asteroid);
        }

        if(_activeAsteroids.Count == 0) // last asteroid is destroyed
            ObserverCollection.Instance.Notify<ILevelEndObserver>((l)=>l.OnLevelEnded());
    }

    private void GenerateAsteroidsAfterDestroy(Asteroid asteroid)
    {
        var mass = asteroid.Settings.Mass;
        var parts = asteroid.Settings.PartIds;
        var force = asteroid.Velocity.magnitude;
        while (mass > 0)
        {
            var id = parts[Random.Range(0, parts.Length)];
            var newVelocity = (Vector2)(Quaternion.Euler(0, 0, Random.Range(-180, 180)) * Vector3.up);
            var spawned = Spawn(id,
                asteroid.Position + newVelocity * asteroid.Size.magnitude * Random.Range(0.1f, 0.7f),
                newVelocity * force);
            mass -= spawned.Settings.Mass;
        }
    }

    public void StartLevel(int number, Level level)
    {
        SpawnLevel(level);
    }
    
    private void Clear()
    {
        foreach (var asteroid in _activeAsteroids)
        {
            if (_asteroids.TryGetValue(asteroid.Settings.Id, out var pool))
            {
                pool.Return(asteroid);
            }
        }
        _activeAsteroids.Clear();
    }

    public void OnGameEnded(bool quit)
    {
        Clear();
    }
}