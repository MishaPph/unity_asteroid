using UnityEngine;

public static class GameScreen
{
    public static readonly Vector2 HalfSize;
    public static readonly Rect ScreenRect;
    
    static GameScreen()
    {
        Application.targetFrameRate = 60;
        if (Camera.main != null)
        {
            HalfSize = new Vector2(Camera.main.orthographicSize * Camera.main.aspect, Camera.main.orthographicSize);
        } else
        {
            HalfSize = new Vector2(Screen.currentResolution.width / 2.0f, Screen.currentResolution.height / 2.0f);
        }
        ScreenRect = new Rect(-HalfSize, HalfSize*2);
    }
}