using System.Collections.Generic;
using Code.Observers;
using TMPro;
using UnityEngine;

public class HUD : MonoBehaviour, IPlayerObserver, ILevelStartObserver
{
    [SerializeField]
    private List<GameObject> _lives = new List<GameObject>();

    [SerializeField] 
    private TextMeshProUGUI _level;

    private void Start()
    {
        ObserverCollection.Instance.Register<IPlayerObserver>(this);
        ObserverCollection.Instance.Register<ILevelStartObserver>(this);
        OnPlayerLifeChanged(0);
    }

    private void OnDestroy()
    {
        ObserverCollection.Instance.Unregister<IPlayerObserver>(this);
        ObserverCollection.Instance.Unregister<ILevelStartObserver>(this);
    }

    public void OnPlayerLifeChanged(int count)
    {
        for (int i = 0; i < _lives.Count; i++)
        {
            _lives[i].SetActive(i < count);
        }
    }

    public void StartLevel(int number, Level level)
    {
        _level.text = $"Level {number + 1}";
    }
}
