using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private Button _exitBtn, _restartBtn, _continueBtn;
    private IPauseMenuListener _listener;
    public bool Active => gameObject.activeSelf;
    public void SetListener(IPauseMenuListener listener) => _listener = listener;

    private void Awake()
    {
        _exitBtn.onClick.AddListener(Exit);
        _restartBtn.onClick.AddListener(Restart);
        _continueBtn.onClick.AddListener(Continue);
    }
    private void Exit()
    {
        _listener.OnPauseExit();
    }

    private void Restart()
    {
        _listener.OnRestart();
    }

    private void Continue()
    {
        _listener.OnPauseContinue();
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}