public interface IPauseMenuListener
{
    void OnPauseExit();

    void OnRestart();

    void OnPauseContinue();
}