using System;
using System.Collections;
using Code.Observers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Button _playBtn;
    [SerializeField] private GameObject _win, _over, _menu;
    [SerializeField] private TextMeshProUGUI _levelComplete;

    private void Start()
    {
        _playBtn.onClick.AddListener(Play);
        _levelComplete.gameObject.SetActive(false);
    }

    public void GameOver()
    {
        gameObject.SetActive(true);
        _over.SetActive(true);
        Invoke(nameof(ShowMenu), 1.0f);
    }

    public void GameWin()
    {
        gameObject.SetActive(true);
        _win.SetActive(true);
        Invoke(nameof(ShowMenu), 1.0f);
    }
    public void HideMenu()
    {
        _menu.SetActive(false);
        _over.SetActive(false);
        _win.SetActive(false);

        gameObject.SetActive(false);
    }

    public void ShowMenu()
    {
        _menu.SetActive(true);
        _over.SetActive(false);
        _win.SetActive(false);

        gameObject.SetActive(true);
    }

    private void Play()
    {
        _menu.SetActive(false);
        _over.SetActive(false);
        _win.SetActive(false);

        gameObject.SetActive(false);

        ObserverCollection.Instance.Notify<IGameStartObserver>(l => l.OnGameStart());
    }

    public void ShowLevelComplete(int level, Action onFinish)
    {
        gameObject.SetActive(true);
        StartCoroutine(PlayComplete(level, onFinish));
    }

    private IEnumerator PlayComplete(int level, Action onFinish)
    {
        _levelComplete.gameObject.SetActive(true);
        _levelComplete.text = $"Level {level} Completed";
        yield return new WaitForSeconds(3.0f);
        _levelComplete.gameObject.SetActive(false);
        onFinish?.Invoke();
        HideMenu();
    }
}