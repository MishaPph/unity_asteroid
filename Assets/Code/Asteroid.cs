using Code.Components;
using Code.Observers;
using Code.Settings;
using UnityEngine;
using Object = UnityEngine.Object;

public class Asteroid : MovableObject, IDamageable
{
    private float _accumulatedDamage = 0;

    public AsteroidSettings Settings;
    
    private bool _isDead = false;
    public bool IsDead => _isDead;
    public float TurnSpeed;

    public Asteroid(GameObject prefab, Vector2 size) : base(prefab, size)
    {
        if (Active.TryGetComponent(out DamageReceiver damageReceiver))
        {
            damageReceiver.Damage += TakeDamage;
            Fake.GetComponent<DamageReceiver>().Damage += TakeDamage;
        }
    }

    public override void Activate()
    {
        base.Activate();
        _accumulatedDamage = 0;
        _isDead = false;
    }

    public void Move(float deltaTime)
    {
        UpdateMove(deltaTime);
        SetRotation(Rotation * Quaternion.Euler(0,0, TurnSpeed * deltaTime));
    }

    private void PlayDestroyFx()
    {
        var prefab = Settings.DestroyedPrefabFx;
        var ps = Object.Instantiate(prefab, Position, Quaternion.identity);
        
        Object.Destroy(ps.gameObject, ps.main.duration);
    }

    public void TakeDamage(float damage)
    {
        if(_isDead)
            return;

        _accumulatedDamage += damage;

        if (_accumulatedDamage >= Settings.Health)
        {
            _isDead = true;
            PlayDestroyFx();
            ObserverCollection.Instance.Notify<IAsteroidDeadObserver>((o) => o.OnAsteroidDead(this));
        }
    }
}