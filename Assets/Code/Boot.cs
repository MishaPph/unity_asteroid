using System.Threading.Tasks;
using Code.Observers;
using Code.Repositories;
using UnityEngine;

public class Boot : MonoBehaviour, 
    IPauseMenuListener, ILevelEndObserver, 
    IEscapeObserver, IGameStartObserver,
    IPlayerDeathObserver
{
    [SerializeField] private Level[] Levels;
    [SerializeField] private PauseMenu _pauseMenu;
    [SerializeField] private MainMenu _mainMenu;
    [SerializeField] private PlayerController _playerPrefab;

    private int _currentLevel = 0;
    public bool HasNextLevel => Levels.Length > _currentLevel;

    public PlayerController Player { get; private set; }

    private void Awake()
    {
        Service.Register<IFactory, Factory>();
        Setup().SafeFireAndForget();
    }

    private async Task Setup()
    {
        _mainMenu.HideMenu();
        await RepositoryManager.Instance.Load<WeaponRepository>();
        await RepositoryManager.Instance.Load<BulletRepository>();
        await RepositoryManager.Instance.Load<ShipRepository>();
        await RepositoryManager.Instance.Load<AsteroidRepository>();
        _mainMenu.ShowMenu();
    }

    private void Start()
    {
        _pauseMenu.SetListener(this);
    }

    private void OnEnable()
    {
        ObserverCollection.Instance.Register<ILevelEndObserver>(this);
        ObserverCollection.Instance.Register<IEscapeObserver>(this);
        ObserverCollection.Instance.Register<IPlayerDeathObserver>(this);
        ObserverCollection.Instance.Register<IGameStartObserver>(this);
    }

    private void OnDisable()
    {
        ObserverCollection.Instance.Unregister<ILevelEndObserver>(this);
        ObserverCollection.Instance.Unregister<IEscapeObserver>(this);
        ObserverCollection.Instance.Unregister<IPlayerDeathObserver>(this);
        ObserverCollection.Instance.Unregister<IGameStartObserver>(this);
    }

    private bool NextLevel()
    {
        if (!HasNextLevel) return false;
        ObserverCollection.Instance.Notify<ILevelStartObserver>((o) => o.StartLevel(_currentLevel, Levels[_currentLevel]));
        _currentLevel++;
        return true;
    }

    public void OnLevelEnded()
    {
        if(!Player.IsAlive)
            return;

        if (HasNextLevel)
        {
            _mainMenu.ShowLevelComplete(_currentLevel, PlayNextLevel);
        }
        else
        {
            _mainMenu.GameWin();
        }
    }

    private void PlayNextLevel()
    {
        if (NextLevel())
        {
            Player.Clear();
            Player.Spawn();
        }
    }

    public void OnPauseExit()
    {
        //show menu
       _mainMenu.ShowMenu();
       _pauseMenu.Hide();
       Player.Clear();

       ObserverCollection.Instance.Notify<IGameEndObserver>((p)=>p.OnGameEnded(true));
    }

    public void OnRestart()
    {
        Player.SetLife(Consts.COUNT_PLAYER_LIFE);
        Player.Spawn();
        _currentLevel = 0;
        NextLevel();
        OnPauseContinue();
    }

    public void OnPauseContinue()
    {
        _pauseMenu.Hide();
        Time.timeScale = 1;
    }

    public void OnEscape()
    {
        if (_pauseMenu.Active)
        {
            OnPauseContinue();
        }
        else
        {
            _pauseMenu.Show();
            Time.timeScale = 0;
        }
    }

    public void OnGameStart()
    {
        if (Player == null)
        {
            Player = Instantiate(_playerPrefab);
            Player.SpawnModel();
            Player.SetLife(Consts.COUNT_PLAYER_LIFE);
        }
        OnRestart();
    }

    public void OnPlayerDied()
    {
        if (!Player.IsAlive)
        {
            Player.Clear();
            //end game
            ObserverCollection.Instance.Notify<IGameEndObserver>((p)=>p.OnGameEnded(false));
            //show hud text
            _mainMenu.GameOver();
        }
        else
        {
            Player.Spawn();
        }
    }
}
