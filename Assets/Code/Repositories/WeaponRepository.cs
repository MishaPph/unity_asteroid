using Code.Enums;
using Code.Settings;

namespace Code.Repositories
{
    public sealed class WeaponRepository : BaseRepository<WeaponSettings, WeaponId>
    {
        protected override string Label => "weapon";
    }
}