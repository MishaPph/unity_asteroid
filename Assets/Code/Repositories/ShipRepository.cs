using Code.Enums;
using UnityEngine;

namespace Code.Repositories
{
    public sealed class ShipRepository : BaseRepository<GameObject, ShipId>
    {
        protected override string Label => "ship";
    }
}