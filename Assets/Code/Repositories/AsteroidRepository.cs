using Code.Enums;
using Code.Settings;

namespace Code.Repositories
{
    public sealed class AsteroidRepository : BaseRepository<AsteroidSettings, AsteroidId>
    {
        protected override string Label => "asteroid";

        protected override AsteroidId GetKey(AsteroidSettings asset) => asset.Id;
    }
}