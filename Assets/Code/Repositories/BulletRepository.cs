using Code.Enums;
using UnityEngine;

namespace Code.Repositories
{
    public sealed class BulletRepository : BaseRepository<GameObject, BulletId>
    {
        protected override string Label => "bullet";
    }
}