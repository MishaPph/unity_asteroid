using System.Threading.Tasks;

namespace Code.Repositories
{
    public interface IRepository
    {
        Task Load();
    }
}