using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Code.Repositories
{
    public class RepositoryManager
    {
        private static RepositoryManager _instance;
        public static RepositoryManager Instance => _instance ??= new RepositoryManager();

        private readonly List<IRepository> Repositories = new List<IRepository>();

        public async Task Load<T>() where T : IRepository
        {
            var repo = (IRepository)Activator.CreateInstance(typeof(T));
            try
            {
                
                await repo.Load();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                throw;
            }
            Repositories.Add(repo);
        }

        public T GetRepository<T>() where T : IRepository
        {
            foreach (var repo in Repositories)
            {
                if (repo is T result)
                    return result;
            }

            Debug.LogError($"Repository {typeof(T).Name} not found");
            return default;
        }

        public bool HasRepository<T>() where T : IRepository
        {
            foreach (var repo in Repositories)
            {
                if (repo is T result)
                    return true;
            }
            return false;
        }
    }
}