using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Object = UnityEngine.Object;

namespace Code.Repositories
{
    public abstract class BaseRepository<T, TK> : IRepository
        where T : Object
        where TK : Enum
    {
        private readonly Dictionary<string, Stack<AsyncOperationHandle>> _handlesByLabel = new();
        private readonly Dictionary<TK, T> _dictionary = new();

        protected abstract string Label { get; }

        private Task<IList<T>> GetAllAssetsWithLabel(string label)
        {
            if (!_handlesByLabel.TryGetValue(label, out var info))
            {
                info = new Stack<AsyncOperationHandle>();
                _handlesByLabel.Add(label, info);
            }

            var handle = Addressables.LoadAssetsAsync<T>(label, null);
            info.Push(handle);
            return handle.Task;
        }

        protected virtual TK GetKey(T asset)
        {
            if (Enum.TryParse(typeof(TK), asset.name.Split('_')[1], true, out var result))
            {
                return (TK)result;
            }

            return default;
        }

        public async Task Load()
        {
            var assets = await GetAllAssetsWithLabel(Label);
            foreach (var asset in assets)
            {
                if (Enum.TryParse(typeof(TK), asset.name.Split('_')[1], true, out var result))
                {
                    _dictionary.Add((TK)result, asset);
                }
            }
        }

        public bool TryGet(TK id, out T settings)
        {
            return _dictionary.TryGetValue(id, out settings);
        }
    }
}