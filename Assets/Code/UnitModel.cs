﻿using System;
using Code.Components;
using UnityEngine;

public class UnitModel : RepeatObject
{
    public event Action<Collider2D> Hit;

    private readonly Animation _animation;
    private readonly Animation _animationFake;

    public UnitModel(GameObject prefab, Vector2 size) : base(prefab, size)
    {
        if (Active.TryGetComponent(out HitBox hitBox))
        {
            hitBox.Hit += OnHit;
            Fake.GetComponent<HitBox>().Hit += OnHit;
        }

        if (Active.TryGetComponent(out _animation))
        {
            _animationFake = Fake.GetComponent<Animation>();
        }
    }

    private void OnHit(Collider2D collider2D)
    {
        Hit?.Invoke(collider2D);
    }

    public void PlayAnimation(string clipName)
    {
        if(_animation == null)
            return;
        _animation.Play(clipName);
        _animationFake.Play(clipName);
    }

    public void StopAnimation()
    {
        if(_animation == null)
            return;
        _animation.Stop();
        _animationFake.Stop();
    }
}
