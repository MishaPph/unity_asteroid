using System;
using UnityEngine;

namespace Code.Components
{
    public class HitBox : MonoBehaviour
    {
        public event Action<Collider2D> Hit;

        private void OnTriggerEnter2D(Collider2D other)
        {
            Hit?.Invoke(other);
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            Hit?.Invoke(other.collider);
        }
    }
}