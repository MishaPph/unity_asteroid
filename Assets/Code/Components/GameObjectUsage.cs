﻿using System;
using Code.Enums;
using UnityEngine;

namespace Code.Components
{
    public class GameObjectUsage : MonoBehaviour
    {
        [Serializable]
        public struct PointInfo
        {
            public TransformUsage Usage;
            public Transform Transform;
        }

        [SerializeField] private PointInfo[] _pointInfo;

        public bool TryGetTransform(TransformUsage usage, out Transform tr)
        {
            for (int i = 0; i < _pointInfo.Length; i++)
            {
                if (_pointInfo[i].Usage != usage) continue;
                tr = _pointInfo[i].Transform;
                return true;
            }

            tr = default;
            return false;
        }
    }
}