using Helpers;
using UnityEngine;

namespace Code.Components
{
    public class RepeatObject : IPoolObject
    {
        protected readonly Transform _activeCacheTransform;
        protected readonly Transform _fakeCacheTransform;

        public Vector2 Position => _activeCacheTransform.localPosition;
        public Quaternion Rotation => _activeCacheTransform.localRotation;
        public GameObject Active { get; }
        public GameObject Fake { get; }
        public Vector2 Size { get; }

        public RepeatObject(GameObject prefab, Vector2 size)
        {
            Size = size;
            Active = Object.Instantiate(prefab);
            _activeCacheTransform = Active.transform;

            Fake = Object.Instantiate(prefab);
            Fake.SetActive(false);
            _fakeCacheTransform = Fake.transform;
        }

        public void SetRotation(Quaternion rotation)
        {
            _activeCacheTransform.localRotation = rotation;
            _fakeCacheTransform.localRotation = rotation;
        }

        public void SetPosition(Vector2 position)
        {
            RepeatHelper.CalculateRepeat(GameScreen.HalfSize, ref position);
            _activeCacheTransform.localPosition = position;
            RefreshFake(position);
        }

        private void RefreshFake(Vector2 position)
        {
            bool objectIsFullyVisible = GameScreen.ScreenRect.Contains(position - Size / 2, Size);
            if (!objectIsFullyVisible)
            {
                _fakeCacheTransform.localPosition =
                    position + RepeatHelper.CalculateOffset(GameScreen.HalfSize, position, Size);
                Fake.SetActive(true);
            }
            else
            {
                Fake.SetActive(false);
            }
        }

        public virtual void Activate()
        {
            Active.SetActive(true);
            Fake.SetActive(false);
        }

        public void Deactivate()
        {
            Active.SetActive(false);
            Fake.SetActive(false);
        }
    }
}