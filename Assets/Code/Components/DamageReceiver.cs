using System;
using UnityEngine;

namespace Code.Components
{
    public class DamageReceiver : MonoBehaviour, IDamageable
    {
        public event Action<float> Damage;

        public void TakeDamage(float damage)
        {
            Damage?.Invoke(damage);
        }
    }
}