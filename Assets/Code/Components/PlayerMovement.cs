using System;
using Code.Settings;
using UnityEngine;

namespace Code.Components
{
    public class PlayerMovement
    {
        public PlayerMovementSettings Settings;

        private float _currentVelocity;
        private float _timeTurningInDirection;
        private float _currentTurnDirectionSignOrZero;
        private float _rotationVelocity;

        public PlayerMovement(PlayerMovementSettings settings)
        {
            Settings = settings;
        }

        public void Stop()
        {
            _currentVelocity = 0;
            _timeTurningInDirection = 0;
            _currentTurnDirectionSignOrZero = 0;
            _rotationVelocity = 0;
        }

        public Vector2 CalculateDeltaPosition(float input, float deltaTime)
        {
            _currentVelocity = CalculateNewVelocityWithAcceleration(_currentVelocity,
                CalculateDesiredVelocity(Mathf.Clamp01(input)), deltaTime);
            return Vector2.up * _currentVelocity;
        }

        public Quaternion CalculateRotation(Quaternion currentRotation, float input, float deltaTime)
        {
            _rotationVelocity = CalculateRotation(_rotationVelocity, input, deltaTime);
            currentRotation *= Quaternion.Euler(0, 0, -_rotationVelocity);
            return currentRotation;
        }

        private float CalculateRotation(float current, float inputValue, float deltaTime)
        {
            var turnDirectionSignOrZero =
                Mathf.Abs(inputValue) <= Settings.NotTurningThreshold ? 0 : Mathf.Sign(inputValue);
            if (Math.Abs(_currentTurnDirectionSignOrZero - turnDirectionSignOrZero) > float.Epsilon)
            {
                _currentTurnDirectionSignOrZero = turnDirectionSignOrZero;
                _timeTurningInDirection = 0;
            }
            else
            {
                _timeTurningInDirection += Time.deltaTime;
            }

            if (Mathf.Abs(inputValue) >= float.Epsilon)
            {
                var velocityDifference = inputValue * Settings.TurnSpeed - current;
                current = Mathf.Clamp(
                    current + velocityDifference * Settings.MaxTurnCurve.Evaluate(_timeTurningInDirection) * deltaTime,
                    -Settings.TurnSpeed, Settings.TurnSpeed);
            }
            else
            {
                current = 0;
            }

            return current;
        }

        private float CalculateDesiredVelocity(float moveInputMagnitude)
        {
            return Settings.Speed * moveInputMagnitude;
        }

        private float CalculateNewVelocityWithAcceleration(float currentVelocity, float desiredVelocity,
            float deltaTime)
        {
            float acceleration = Settings.AccelerationCurve.Evaluate(currentVelocity);
            float deceleration = Settings.DecelerationCurve.Evaluate(currentVelocity);

            float accelerationOrDeceleration =
                (desiredVelocity >= currentVelocity - float.Epsilon) ? acceleration : deceleration;
            var velocityDifference = desiredVelocity - currentVelocity;
            var current = currentVelocity + Mathf.Clamp(velocityDifference * accelerationOrDeceleration * deltaTime,
                -Settings.Speed, Settings.Speed);
            return current;
        }
    }
}