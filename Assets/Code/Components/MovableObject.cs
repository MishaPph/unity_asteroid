using UnityEngine;

namespace Code.Components
{
    public class MovableObject : RepeatObject
    {
        public Vector2 Velocity = Vector2.zero;

        protected MovableObject(GameObject prefab, Vector2 size) : base(prefab, size)
        {
            if (Active.TryGetComponent(out HitBox hitBox))
            {
                hitBox.Hit += OnHit;
                Fake.GetComponent<HitBox>().Hit += OnHit;
            }
        }

        public void SetTransform(Vector3 position, Quaternion rotation)
        {
            _activeCacheTransform.localPosition = position;
            _activeCacheTransform.localRotation = rotation;

            _fakeCacheTransform.localPosition = position;
            _fakeCacheTransform.localRotation = rotation;
            SetPosition(position);
        }

        protected virtual void OnHit(Collider2D collider2D)
        {
        }

        protected void UpdateMove(float deltaTime)
        {
            var position = _activeCacheTransform.localPosition;
            position += new Vector3(Velocity.x * deltaTime, Velocity.y * deltaTime);
            SetPosition(position);
        }
    }
}