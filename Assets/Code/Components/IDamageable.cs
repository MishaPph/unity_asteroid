namespace Code.Components
{
    public interface IDamageable
    {
        void TakeDamage(float damage);
    }
}