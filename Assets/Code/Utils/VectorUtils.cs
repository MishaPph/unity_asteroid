using UnityEngine;

namespace Utils
{
    public static class VectorUtils
    {
        public static Vector2 XZ(this Vector3 vector3)
        {
            return new Vector2(vector3.x, vector3.z);
        }
        public static Vector3 X0Z(this Vector2 vector2)
        {
            return new Vector3(vector2.x, 0, vector2.y);
        }
    }
}