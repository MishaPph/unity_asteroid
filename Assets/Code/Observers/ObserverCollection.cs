using System;
using System.Collections.Generic;
using UnityEngine;

namespace Code.Observers
{
    public class ObserverCollection
    {
        private static ObserverCollection _instance;
        public static ObserverCollection Instance => _instance ??= new ObserverCollection();

        private readonly Dictionary<Type, List<IObserver>> _items = new();

        public void Register<T>(T observer) where T : IObserver
        {
            if (!_items.ContainsKey(typeof(T)))
                _items.Add(typeof(T), new List<IObserver>());
            _items[typeof(T)].Add(observer);
        }

        public void Unregister<T>(T observer) where T : IObserver
        {
            if (!_items.ContainsKey(typeof(T)))
                return;
            _items[typeof(T)].Remove(observer);
        }

        public void Notify<T>(Action<T> action) where T : IObserver
        {
            if (!_items.TryGetValue(typeof(T), out var collection))
                return;

            foreach (var observer in collection)
            {
                var item = (T)observer;
                try
                {
                    action(item);
                }
                catch (Exception ex)
                {
                    Debug.LogException(ex);
                }
            }
        }

        public void Clear()
        {
            _items.Clear();
        }
    }
}