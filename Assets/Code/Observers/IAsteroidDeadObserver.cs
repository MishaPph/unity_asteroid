
namespace Code.Observers
{
    public interface IAsteroidDeadObserver : IObserver
    {
        void OnAsteroidDead(Asteroid asteroid);
    }
}
