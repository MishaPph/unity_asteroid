
namespace Code.Observers
{
    public interface IPlayerObserver : IObserver
    {
        void OnPlayerLifeChanged(int count);
    }
}