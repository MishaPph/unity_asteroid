
namespace Code.Observers
{
    public interface ILevelStartObserver : IObserver
    {
        void StartLevel(int number, Level level);
    }
}