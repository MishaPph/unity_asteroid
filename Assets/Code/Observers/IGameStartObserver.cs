
namespace Code.Observers
{
    public interface IGameStartObserver : IObserver
    {
        void OnGameStart();
    }
}