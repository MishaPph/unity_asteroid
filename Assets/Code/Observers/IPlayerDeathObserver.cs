
namespace Code.Observers
{
    public interface IPlayerDeathObserver : IObserver
    {
        void OnPlayerDied();
    }
}