
namespace Code.Observers
{
    public interface IGameEndObserver : IObserver
    {
        void OnGameEnded(bool quit);
    }
}