
namespace Code.Observers
{
    public interface ILevelEndObserver : IObserver
    {
        void OnLevelEnded();
    }
}