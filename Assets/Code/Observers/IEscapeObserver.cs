
namespace Code.Observers
{
    public interface IEscapeObserver : IObserver
    {
        void OnEscape();
    }
}