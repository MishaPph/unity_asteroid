using System.Collections;
using Code.Components;
using Code.Enums;
using Code.Observers;
using Code.Settings;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(PlayerInput))]
public class PlayerController : MonoBehaviour
{
    public PlayerMovementSettings Settings;
    public GameObjectUsage Usage { get; private set; }
    public UnitModel Model { get; private set; }
    public WeaponController WeaponController { get; private set; }

    public PlayerMovement Movement;
    private Vector2 _mMove;

    private int _life = 0;
    public bool IsAlive => _life > 0;
    public bool Invulnerable { get; private set; }
    private Coroutine _invulnerableCoroutine;

    public void SpawnModel()
    {
        Model = Service.Get<IFactory>().CreateShip(ShipId.Yellow);
        Usage = Model.Active.GetComponent<GameObjectUsage>();
        Movement = new PlayerMovement(Settings);
        WeaponController = new WeaponController(Usage);

        Model.Hit += ModelOnHit;
        SetLife(_life);
    }

    public void Spawn()
    {
        ReturnToStart();
        Model.Activate();
        _invulnerableCoroutine = StartCoroutine(StartInvulnerability());
    }

    private void ModelOnHit(Collider2D obj)
    {
        if(Invulnerable)
            return;
        SetLife(_life - 1);
        WeaponController.Firing(false);
        Movement.Stop();
        Model.Deactivate();
        ObserverCollection.Instance.Notify<IPlayerDeathObserver>((p)=>p.OnPlayerDied());
    }

    private IEnumerator StartInvulnerability()
    {
        Invulnerable = true;
        Model.PlayAnimation("blink");
        yield return new WaitForSeconds(2.0f);
        StopInvulnerability();
    }

    private void StopInvulnerability()
    {
        if (_invulnerableCoroutine != null)
        {
            StopCoroutine(_invulnerableCoroutine);
            _invulnerableCoroutine = null;
        }
        Invulnerable = false;
        Model.PlayAnimation("default");
    }
    
    private void ReturnToStart()
    {
        Model.SetPosition(Vector2.zero);
        Model.SetRotation(Quaternion.identity);
        StopInvulnerability();
    }

    public void SetLife(int count)
    {
        _life = count;
        ObserverCollection.Instance.Notify<IPlayerObserver>((p)=>p.OnPlayerLifeChanged(count));
    }

    public void Clear()
    {
        Model.Deactivate();
        WeaponController.Clear();
        Movement.Stop();
    }

    public void OnFire(InputValue value)
    {
        if(!IsAlive)
            return;
        WeaponController.Firing(value.isPressed);
    }

     public void OnMove(InputValue value)
     {
         _mMove = value.Get<Vector2>();
     }

     public void OnNextWeapon()
     {
         WeaponController.Next();
     }

     public void OnPreviousWeapon()
     {
         WeaponController.Previous();
     }

     public void OnEscape()
     {
         ObserverCollection.Instance.Notify<IEscapeObserver>((p)=>p.OnEscape());
     }

     public void Update()
     {
         WeaponController.Update(Time.deltaTime);
         
         if(!IsAlive)
             return;
         var rotation = Movement.CalculateRotation(Model.Rotation, _mMove.x, Time.deltaTime);
         var position = Movement.CalculateDeltaPosition(_mMove.y, Time.deltaTime);

         Model.SetRotation(rotation);
         Model.SetPosition(Model.Position + (Vector2)(rotation * position) * Time.deltaTime);
     }

     private void OnDrawGizmos()
     {
         if(Movement == null)
             return;
        Gizmos.DrawWireCube(Model.Position, Model.Size);
     }
}
