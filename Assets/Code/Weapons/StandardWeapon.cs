using System.Collections.Generic;
using Code.Components;
using Code.Enums;
using Code.Settings;
using UnityEngine;

public class StandardWeapon : IWeapon
{
    private readonly List<Bullet> _bullets = new();
    private readonly PoolObject<Bullet , BulletId> _pool;
    private readonly Transform _bulletPoint;
    private float _readyToFire = -1;
    private bool _inFire;

    public int CountActiveBullets => _bullets.Count;
    public WeaponSettings Settings { get; }
    public bool CanFire => _readyToFire  < Time.time;

    public StandardWeapon(WeaponSettings setting, Transform root)
    {
        Settings = setting;
        _bulletPoint = root;
        _pool = new PoolObject<Bullet, BulletId>(setting.BulletId, CreateBullet);
    }

    private Bullet CreateBullet(BulletId id)
    {
        var b = Service.Get<IFactory>().CreateBullet(id);
        b.Triggered += OnBulletTriggered;
        return b;
    }

    public void StopFire() => _inFire = false;
    public void StartFire()
    {
        _inFire = true;
        Fire();
    }

    private void Fire()
    {
        if(!CanFire)
            return;

        _readyToFire = Time.time + Settings.CoolDown;
        //create bullet
        SpawnBullet();
    }

    private Bullet SpawnBullet()
    {
        var bullet = _pool.Create();
        bullet.SetTransform(_bulletPoint.position, _bulletPoint.rotation);
        bullet.Life = Settings.BulletLifeTime;
        bullet.Velocity =  _bulletPoint.up * Settings.BulletSpeed;
        _bullets.Add(bullet);
        return bullet;
    }
    
    private void PlayHitFx(Vector3 position)
    {
        var prefab = Settings.HitPrefabFx;
        var ps = Object.Instantiate(prefab, position, Quaternion.identity);
        Object.Destroy(ps.gameObject, ps.main.duration);
    }

    private void OnBulletTriggered(Bullet bullet, Collider2D collider)
    {
        if (collider.TryGetComponent(out IDamageable damageable))
        {
            PlayHitFx(bullet.Position);
            damageable.TakeDamage(Settings.Damage);
        }
    }

    public void Update(float deltaTime)
    {
        if(_inFire)
            Fire();
        for (var i = _bullets.Count - 1; i >= 0; i--)
        {
            if (_bullets[i].Move(deltaTime)) continue;
            _pool.Return(_bullets[i]);
            _bullets.RemoveAt(i);
        }
    }

    public void Clear()
    {
        foreach (var bullet in _bullets)
        {
            _pool.Return(bullet);
        }
        _bullets.Clear();
    }
}