
public interface IWeapon
{
    int CountActiveBullets { get; }
    void StartFire();
    void StopFire();
    void Update(float deltaTime);
    void Clear();
}