using System.Collections.Generic;
using Code.Components;
using Code.Enums;

public class WeaponController
{
    private IWeapon Active => _weapons[_index];

    private readonly List<IWeapon> _weapons = new ();
    private int _index;
    private bool _firing;

    public WeaponController(GameObjectUsage usage)
    {
        if (usage.TryGetTransform(TransformUsage.BulletPoint, out var bulletTransform))
        {
            var factory = Service.Get<IFactory>();
            _weapons.Add(factory.CreateWeapon(WeaponId.Standard, bulletTransform));
            _weapons.Add(factory.CreateWeapon(WeaponId.Fast, bulletTransform));
            _weapons.Add(factory.CreateWeapon(WeaponId.Slow, bulletTransform));
        }
    }

    public void Firing(bool enable)
    {
        _firing = enable;
    }

    public void Update(float deltaTime)
    {
        foreach (var weapon in _weapons)
        {
            weapon.Update(deltaTime);
        }
        if (_firing)
        {
            Active.StartFire();
        } else 
            Active.StopFire();
    }

    public void Next()
    {
        Active.StopFire();
        _index++;
        _index %= _weapons.Count;
    }

    public void Previous()
    {
        Active.StopFire();
        _index += _weapons.Count - 1;
        _index %= _weapons.Count;
    }

    public void Clear()
    {
        //remove all bullets
        foreach (var weapon in _weapons)
        {
            weapon.Clear();
        }
    }
}