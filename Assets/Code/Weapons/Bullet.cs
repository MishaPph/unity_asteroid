using System;
using Code.Components;
using UnityEngine;

public class Bullet : MovableObject
{
    public float Life;

    public event Action<Bullet, Collider2D> Triggered;

    public Bullet(GameObject prefab, Vector2 size) : base(prefab, size)
    {
    }

    public bool Move(float deltaTime)
    {
        UpdateMove(deltaTime);
        Life -= deltaTime;
        return Life > 0;
    }

    protected override void OnHit(Collider2D other)
    {
        if(Life <= 0)
            return;
        Life = -1;
        Triggered?.Invoke(this, other);
    }
}