namespace Code.Enums
{
    public enum AsteroidId
    {
        Small,
        Middle,
        Big,
        Huge
    }
}