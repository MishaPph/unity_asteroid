using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(MinMaxSliderAttribute))]
public class MinMaxSliderDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        Rect controlRect = EditorGUI.PrefixLabel(position, label);
        
        Rect[] splittedRect = SplitRect(controlRect, 3);

        MinMaxSliderAttribute range = attribute as MinMaxSliderAttribute;
        var value = property.vector2Value;

        value.x = EditorGUI.FloatField(splittedRect[0], value.x);
        value.y = EditorGUI.FloatField(splittedRect[2], value.y);
        EditorGUI.MinMaxSlider(splittedRect[1], ref value.x, ref value.y, range.Min, range.Max);
        property.vector2Value = value;
    }

    private static Rect[] SplitRect(Rect rectToSplit, int n)
    {
        Rect[] rects = new Rect[n];

        for(int i = 0; i < n; i++)
        {
            rects[i] = new Rect(rectToSplit.position.x + (i * rectToSplit.width / n), rectToSplit.position.y, rectToSplit.width / n, rectToSplit.height);
        }

        int padding = (int)rects[0].width - 60;
        int space = 5;

        rects[0].width -= padding + space;
        rects[2].width -= padding + space;

        rects[1].x -= padding;
        rects[1].width += padding * 2;

        rects[2].x += padding + space;
        return rects;
    }
}